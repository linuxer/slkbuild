.TH "SLKBUILD" 5 "%%mtime(%m/%d/%Y)" "linuxer"

.SH NAME
.P
SLKBUILD \- slkbuild meta\-file
.SH DESCRIPTION
.P
\fBSLKBUILD\fRs are the meta\-files fed to \fIslkbuild\fR during automatic
build script creation. This manual page will give a run down of all the
possible options that can be included in a \fBSLKBUILD\fR and an explanation
of each.
.SH MANDATORY VARIABLES AND FUNCTIONS

.TP
\fBpkgname\fR
The name of the package. Per packaging standards, keep it all lowercase.
.TP
\fBpkgver\fR
The version of the software. No dashes are allowed. For packages
created from svn, you can use "r12345",
if you checked out revision 12345. For cvs and git packages you
can use the current date in the form YEARMONTHDAY, e.g.
"20100115". Alpha and beta releases can be designated as such by
appending the alpha or beta versions to the package, e.g.
"1.0beta5" or "1.0b5", usually in the same way the source
package is versioned, keeping in mind that no dashes are allowed.
.TP
\fBpkgrel\fR
This is the release number of the package. A release number is the
number of times this package has been created for this particular
\fIpkgver\fR. So the first time you package a piece of software at a
particular version, it is \fIpkgrel\fR 1, if you package it again at that
exact same version perhaps to use different compile options or some
other reason, it is \fIpkgrel\fR 2. However, if the package version changes,
you go back to 1. So for example, software foo is at version 1.0
and is packaged, so that is \fIpkgrel\fR 1. It is packaged again at
version 1.0 because of bad \fICXXFLAGS\fR, that one is called \fIpkgrel\fR 2.
If the software advances to 2.0, and you package that, the \fIpkgrel\fR
goes back to 1, and doesn't advance to 3. It's customary to append
the packagers initials to the package release number when building
packages that are not official Slackware packages, e.g. "1gv".
.TP
\fBsource\fR
This is an array for the source files needed to complete the build. This
can be anything from a url to a tarball to a local personally created
patch or .desktop file. If you have more than one, just put each one in
quotes and make sure there is a space between them and make sure they
are inside the parentheses. E.g.:

.nf
source=("http://url.tar.gz" "gcc.path" "thing.desktop")
.fi


For cvs, svn and git builds, this variable is not technically necessary,
you can leave it empty and fetch the svn or cvs source in
\fIbuild()\fR. Or you can create a source tarball from cvs, svn or git
and use that in the source array.
.TP
\fBslackdesc\fR
This is an array where you put the description of the software you are
packaging. On the first line put the package name followed by a space a
dash and another space and then a short description. On the following
1\-9 lines dependending on how much you need, put a longer explanation.
Enclose each line in quotes and make sure each line is under 70
characters, which can be ensured by following the handy ruler provided
in the \fISLKBUILD\fR prototype created by \fBslkbuild \-\-generate\fR. For those
used to skipping the second line of the slackdesc, the script does that
automatically so go straight into the long explanation in the second
line.
.TP
\fBbuild()\fR
This is the function that is executed which actually does all the
compiling. Before this function is ran, the path to the current
directory is assigned the variable \fI$startdir\fR and then
\fI$startdir/src\fR is
created. After that anything specified in the \fIsource=()\fR array is copied
into \fI$startdir/src\fR and all tarballs are untarred. After that,
\fI$startdir/pkg\fR is created. Then this function is run. So the aim of this
function is to put whatever commands that are necessary, be it
\fI./configure\fR,
\fImake\fR, \fImake install DESTDIR=$startdir/pkg\fR, or any other
combination of commands, to setup \fI$startdir/pkg\fR correctly.
\fI$startdir/pkg\fR
needs to be setup as if it is the / and so everything underneath goes
where it is supposed to. If for instance, you are packaging something
that you want to go in \fI/usr/bin/foo\fR, you would make sure it got to
\fI$startdir/pkg/usr/bin/foo\fR in this function. The environment variable
\fBnumjobs\fR
is used in \fIbuild()\fR to specify the default number of jobs make is going
to use if "make \-j $numjobs" is specified in \fIbuild()\fR.
.TP
\fBarch\fR
The architecture of the build. You usually don't need to set this
yourself, unless you want to create a "noarch" package. For 32\-bit
architectures, it will be automatically set to "i586" if not explicitly
specified otherwise. For 64\-bit architectures it will be automatically
set to "x86_64". For ARM architectures it will be automatically set to
"arm". This will automatically set the correct \fICFLAGS\fR and
\fICXXFLAGS\fR in any case, "\-O2 \-march=$arch \-mtune=i686" for 32\-bits,
//"\-O2 \-fPIC" for 64\-bits and "\-O2" for arm respectively. See also
\fICFLAGS/CXXFLAGS\fR below.
.TP
\fBdocs\fR
This is used in almost every package, but might possibly not be used.
You fill in whatever documentation that comes with the package. Put:

.nf
docs=("AUTHORS" "LICENSE" "Changelog")
.fi


into this variable and these files
will be moved over appropriately during the build to be included in
the package. Don't worry about specifiying any path or the case of
the docs because \fBslkbuild\fR does a case insensitive recursive search
for whatever you provide. If you have more than one, do like I did
above or like you do in source and put each doc in quotes with a
space between them.
.TP
\fBoptions\fR
This is an array that takes certain arguments and affects how the build
script operates. The options it takes are \fIkeepla\fR, \fInoextract\fR,
\fInostrip\fR, \fInoautodotnew\fR, \fInosrcpack\fR, \fInolinkprepend\fR, \fItxz\fR,
\fItgz\fR, \fItlz\fR, \fItbz\fR and \fItbr\fR. \fIkeepla\fR, will override the default
behaviour of automatically removing libtool archives (.la files).
\fInoextract\fR will prevent the build script from extracting tarballs
automatically which is the default behavior. This is useful for
those pesky tarbombs which explode all the contents into the
current directory when extracted instead of its own dedicated
directory and has some other potential uses. If you use this
option, you must extract the tarballs yourself in \fIbuild()\fR.
\fInostrip\fR prevents the execution of the stripping function, can be
useful in some applications, that break if they are stripped.
\fInoautodotnew\fR is used to remove the automatic dotnew handling in
all files in etc. \fInosrcpack\fR will not include any source files in
the resulting package, even if they are specified without a url
in the source array. \fInolinkprepend\fR will create the package by
running makepkg without the \fB\-p\fR switch, which is the default
behaviour. The options \fItxz\fR, \fItgz\fR, \fItlz\fR, \fItbz\fR and \fItbr\fR
set the compression format for the resulting package, the default
being \fItxz\fR.
If you set more than one of \fItxz\fR, \fItgz\fR, \fItlz\fR, \fItbz\fR or
\fItbr\fR, only the first one will be used, whichever that is.
.TP
\fBsourcetemplate\fR
This is a template to where the source will be put online (the ones that
don't contain a full url). E.g.:

.nf
sourcetemplate=http://yourepo.org/$pkgname/$pkgver
.fi


If sourcetemplate is not set, an \fI.src\fR file will not be created.
.TP
\fBurl\fR
A homepage or some other information url about the software.
.TP
\fBdotnew\fR
These are generally configuration files that you don't want to overwrite
during install because they will mess up any configuration that the user
installing the package already had setup. You put the path that they
will be on the filesystem. So let's say you don't want \fI/etc/foo.conf\fR to
be overwritten and you are packaging a new version of software foo. You
put:

.nf
dotnew=('etc/foo.conf')
.fi


and that file in the package (in
\fI$startdir/pkg\fR) will get moved to \fIfoo.conf.new\fR and a test will be
put in the \fIdoint.sh\fR (script that runs after install) to see whether or
not to move it over.
.TP
\fBCFLAGS/CXXFLAGS\fR
The script already assigned appropriate flags for the project according
the the specified architecture (see \fIarch\fR above), if for some reason
those aren't the ones that you want, you can override them using these
variables.
.TP
\fBdoinst()\fR
This is the script that runs after a package is installed, so put any
commands in here that you wan't to be run after the package is
installed.
.TP
\fBCustom Variables\fR
Custom variables can also be added, so that they are used in the
\fIbuild()\fR function. These variables should have a name that starts
with an underscore.

.SS AUTHORS
.P
George Vlahavas <vlahavas~at~gmail~dot~com>.

Any revisions will be done by Linuxer <linuxer-at-disroot-dot-org>.
.SS SEE ALSO
.P
\fBslkbuild\fR(8)

