slkbuild is a fork of buildpkg

(previous releases at at https://github.com/gapan/slkbuild)

Here is a list of people who have contributed to buildpkg,
arranged in alphabetical order:

- George Vlahavas <vlahavas~at~gmail~dot~com>

- Matthew Bruenig <matthewbruenig@gmail.com>

- Pedro Pinto <ei10066@alunos.ipb.pt>

Licenced under GLP-2.0 terms

SLKBUILD examples at https://people.salixos.org/
